SELECT 
	aa.doi, 
	unnest(cc.abstract) abstract,
	case 
		when 'Review' = any(aa.item_type) then 'review'
	else 'notreview'
	end orig_class
FROM scp_b_202310.items aa
left join scp_b_202310.abstracts cc on aa.item_id = cc.item_id
where aa.pmid is null and cc.abstract is not null and aa.pubyear >= 2019
order by random()
limit 1000;