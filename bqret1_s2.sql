SELECT externalids.DOI AS doi, abstract, 
    CASE 
        WHEN 'Review' IN UNNEST(publicationtypes) THEN 'review'
        ELSE 'notreview'
    END AS orig_class
FROM subugoe-wag-closed.S2AG.papers_2023_09_26 AS s2_papers
LEFT JOIN subugoe-wag-closed.S2AG.abstracts_2023_09_26 AS s2_abstracts
ON s2_papers.corpusid = s2_abstracts.corpusid
WHERE abstract IS NOT NULL AND year >= 2019 AND externalids.DOI IS NOT NULL AND externalids.PubMed IS NULL AND externalids.PubMedCentral IS NULL
ORDER BY RAND()
LIMIT 1000
