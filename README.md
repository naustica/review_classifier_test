# Review classifier test
*30.01.2024*

This little project is used to validate the results of a [document type classifier based on SetFit](https://huggingface.co/almugabo/review_classifier) against the document type classifications provided by Web of Science and Scopus. In addition, the classifier is validated against a set of publications from review journals which are assumed to be reviews by definition. The original classifier is based on abstracts and was developed by almugabo.

*Note on classification:* Rember that there is no right or wrong but rather one classification system against another!

## Descritpion
This project represents the full pipeline of retrieving and checking classifications in python. It throws some custom SQL queries to the database of the [German Competence Network Bibliometrics](www.bilbiometrie.info) to get document classifications based on various filters. Since the SetFit model was trained on PubMed data, PubMed indexed articles should only be used for cross-validation but not for evaluating the model.


## How to use
1. Add your KB access data as "databasecreds.py" (see databasecredsDUMMY.py). If you don't have KB access, you can send a request with filters of interest.
2. write an SQL query to get kb data. The resulting table should feature a column named "abstract" with the string of the abstract to be classified, and a column named "orig_class" with the original classification by WoS, Scopus or else. The query can be stored as sql file and imported with
``` YourSqlQuery = open("filename.sql", "r").read()```

3. Run ```getClassifyAnalyze("QueryAlias", YourSqlQuery)``` on a particular query and you get the results in the terminal

